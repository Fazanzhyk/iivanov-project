document.addEventListener('DOMContentLoaded', onReady);

function onReady () {
    let IpBtn = document.createElement('button');
    IpBtn.id = 'ip-button';
    IpBtn.innerHTML = 'Вычислить по IP';
    document.body.append(IpBtn);
    IpBtn.addEventListener('click', onIpBtnClick);
}

async function onIpBtnClick () {
    const xhr = new Request();
    await xhr.getByFetch('https://api.ipify.org/?format=json', getIp);
}

async function getIp (data) {
    const ipReq = new Request();
    await ipReq.getByFetch(`http://ip-api.com/json/${data.ip}?fields=continent,country,region,regionName,city,district&lang=ru`, buildDom);
}

function buildDom (data) {
    const ul = document.createElement('ul');
    ul.id = 'list';
    const list = `Континент:<li>${data.continent}</li>
                  Страна:<li>${data.country}</li>
                  Регион:<li>${data.region}</li>
                  Название_Региона:<li>${data.regionName}</li>
                  Город:<li>${data.city}</li>
                  <li>${data.district}</li>`;
    ul.innerHTML = list;
    document.body.append(ul);
}
