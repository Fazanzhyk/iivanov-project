document.addEventListener('DOMContentLoaded', onReady);

let step = 0;

function onReady () {
    let cookieBtn = document.getElementById('cookie-btn');
    cookieBtn.addEventListener('click', onCookieBtnClick);
}

function onCookieBtnClick () {
    step++;
    // deleteCookie('new-user');
    // deleteCookie('experiment');
    setCookie('experiment', 'no-value', {'max-age': 5000});
    if (step === 1) {
        setCookie('new-user', 'true');
    }
    if (step !==1) {
        setCookie('new-user', 'false');
    }
}

function setCookie(name, value, options = {}) {

    options = {
        path: '/',
    };

    // if (options.expires.toUTCString) {
    //     options.expires = options.expires.toUTCString();
    // }

    let updatedCookie = encodeURIComponent(name) + "=" + encodeURIComponent(value);

    for (let optionKey in options) {
        updatedCookie += "; " + optionKey;
        let optionValue = options[optionKey];
        if (optionValue !== true) {
            updatedCookie += "=" + optionValue;
        }
    }

    document.cookie = updatedCookie;
}

// function getCookie(name) {
//     let matches = document.cookie.match(new RegExp(
//         "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
//     ));
//     return matches ? decodeURIComponent(matches[1]) : undefined;
// }

function deleteCookie(name) {
    setCookie(name, "", {
        'max-age': -1
    })
}