document.addEventListener('DOMContentLoaded', onReady);

const reqFetch = new Request();

function onReady() {
    let starWars = document.createElement('button');
    starWars.innerHTML = 'STAR WARS';
    starWars.id = 'star-wars';
    document.body.append(starWars);
    starWars.addEventListener('click', onStarWarsClick);
}

function onStarWarsClick (e) {
    let target = e.target;
    reqFetch.getByFetch('https://swapi.co/api/films/', onGetByFetch);
    target.style.display = 'none';
}

function onGetByFetch (data) {

    const ul = document.createElement('ul');
    document.body.append(ul);

    data.results.forEach(item => {
        const li = document.createElement('li');
        const {characters, title, episode_id, opening_crawl} = item;

        li.innerHTML = `<strong>${episode_id}:${title}</strong>
                          <div>${opening_crawl}</div>`;
        ul.append(li);

        const ulCharacter = document.createElement('ul');
        li.append(ulCharacter);

        let html = '';
        const restItems = Promise.all(getCharacters(characters)).then(obj => obj.forEach(name => {
            html += `<li>${name}</li>`;
            ulCharacter.innerHTML = html;
        }));

        console.log(restItems);

    })
}

function getCharacters (urls) {
    const arrResults = [];

    for (let i = 0; i < urls.length; i++) {
        arrResults.push(
            fetch(urls[i], {
                type: 'GET'
            }).then(response=>response.json())
                .then(character => character.name)
        );
    }
    return arrResults;
}
