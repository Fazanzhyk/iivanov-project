document.addEventListener('DOMContentLoaded', onReady);

const ajax = new Request();

function onReady() {
    let starWars = document.createElement('button');
    starWars.innerHTML = 'STAR WARS';
    starWars.id = 'star-wars';
    document.body.append(starWars);
    starWars.addEventListener('click', onStarWarsClick);
}

function onStarWarsClick (e) {
    let target = e.target;
    ajax.getData('https://swapi.co/api/films/', fnOnFilmHandler);
    target.style.display = 'none';
}

function fnOnFilmHandler(data) {

    const {results} = data;

    const ul = document.createElement('ul');
    document.body.append(ul);

    results.forEach(item => {

        const li = document.createElement('li');
        const {characters, title, episode_id, opening_crawl} = item;

        li.innerHTML = `<strong>${episode_id}:${title}</strong>
                          <div>${opening_crawl}</div>`;
        ul.append(li);

        const ulCharacter = document.createElement('ul');
        li.append(ulCharacter);

        let html = '';


        let arrReq = [];
        for (let i = 0; i < characters.length; i++) {
            arrReq.push(
               ajax.getData(characters[i], (character) => {
                    html += `<li>${character.name}</li>`;
                })
            );
        }


        let count = 0;
        arrReq.forEach(request => {
            request.addEventListener('load', function () {
                    count++;

                    if (arrReq.length === count) {
                        ulCharacter.innerHTML = html;
                    }
                }
            );
        });
    })
}

