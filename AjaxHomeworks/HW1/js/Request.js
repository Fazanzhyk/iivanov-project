class Request {

    getData(url, callback) {
        const req = new XMLHttpRequest();
        req.open('GET', url);
        req.onload = function () {
            callback(req.response);
        };
        req.responseType = 'json';
        req.send();

        return req;
    }

    getByFetch (url, callback) {
        fetch (url, {
            type: 'GET'
        })
            .then (response => {response.json().then(results => {
                callback(results);})})
            .catch (reject => {console.log(reject)})
    }

}

