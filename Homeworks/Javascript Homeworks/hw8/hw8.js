document.addEventListener('DOMContentLoaded', onReady);

const input = document.createElement('input');
let spn = document.getElementById('spanner');
let btn = document.getElementById('buttoner');
const txt = 'Please enter correct price';
const crt = document.createTextNode(txt);


/**
 * ������� ��������� ����� ��� �������� �������� � ���������� �� ���� 2 �����������
 *
 */
function onReady () {
    input.placeholder = 'Price';
    input.addEventListener('focus', onFocus);
    input.addEventListener('blur', onBlur);
    document.body.appendChild(input);
}

/**
 * ���������� ������� ��� ����������� �� �����
 *
 */
function onFocus () {
    input.style.borderColor = 'green';
    document.body.removeChild(crt);
}

/**
 * ���������� ������� ��� ������ ������� ������
 *
 */
function onBlur () {
    input.style.borderColor = 'initial';
    if (!spn && !btn) {
        spn = document.createElement('span');
        spn.id = 'spanner';
        btn = document.createElement('button');
        btn.id = 'buttoner';
        btn.innerHTML = 'X';
        btn.style.color = 'red';
        input.style.color = 'green';
    }
    input.style.display = 'block';
    btn.addEventListener('click', btnOnClick);
    document.body.insertBefore(spn, input);
    document.body.insertBefore(btn, input);
    const val = this.value;
    spn.innerHTML = `Current Price: ${val}`;
    if (val<0) {
        document.body.removeChild(spn);
        document.body.removeChild(btn);
        input.style.borderColor = 'red';
        document.body.appendChild(crt);
    }
}

/**
 * ���������� ������� ��� ����� �� ������
 *
 */
function btnOnClick () {
    document.body.removeChild (spn);
    document.body.removeChild (btn);
    input.value = null;
}

