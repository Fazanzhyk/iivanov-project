$(document).ready(function () {
    $('.tabs').on('click', function (event) {
        const dataTab = $(event.target).attr('data-tab');
        for (let i = 0; i < $('.tabs-title').length; i++) {
            $('.tabs-title').eq(i).removeClass('active');
        }
        $(event.target).addClass('active');
        for (let i = 0; i < $('.tabs-body').length; i++) {
         (dataTab==i)?$('.tabs-body').eq(i).css('display', 'block'):$('.tabs-body').eq(i).css('display', 'none');
     }
    })
});