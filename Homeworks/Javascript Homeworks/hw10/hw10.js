document.addEventListener('DOMContentLoaded', onReady);

const eyes = document.getElementsByClassName('fas');
const inpts = document.getElementsByTagName('input');
const btn = document.getElementsByClassName('btn');
let spn = document.getElementById('spanner');
const lbl = document.getElementsByClassName('input-wrapper');



/**
 * ���������� ����������� 3 �������.
 *
 */
function onReady () {
    eyes[1].classList.remove('fa-eye-slash');
    eyes[1].classList.add('fa-eye');
    for (let eye of eyes) {
        eye.addEventListener('click', onEyeClick);
    }
    btn[0].setAttribute('type', 'button');
    btn[0].addEventListener('click', onBtnClick);
    for (let inpt of inpts) {
        inpt.addEventListener('focus', onInptFocus);
    }
}

/**
 * 1-� �������. ���������� ����� �� ������
 *
 * @param event
 */
function onEyeClick (event) {
    const eye =  event.target;
    const dataId = event.target.getAttribute('data-id');
       if (eye.className === 'fas fa-eye icon-password' || eye.className === 'fas icon-password fa-eye') {
           eye.classList.remove('fa-eye');
           eye.classList.add('fa-eye-slash');
           if (dataId == 1) {
               inpts[0].setAttribute('type', 'text');
           } else {
               inpts[1].setAttribute('type', 'text');
           }
       } else {
           eye.classList.remove('fa-eye-slash');
           eye.classList.add('fa-eye');
           if (dataId == 1) {
               inpts[0].setAttribute('type', 'password');
           } else {
               inpts[1].setAttribute('type', 'password');
           }
       }
}

/**
 * ������ �������. ���������� ������� �� ������.
 *
 * @param event
 */
function onBtnClick (event) {
         btn[0] = event.target;
         if (inpts[0].value === inpts[1].value) {
             alert ('You are welcome');
         } else {
             if (!spn) {
                spn = document.createElement('p');
                spn.id = 'spanner';
                spn.innerHTML = 'You need to input same values';
                spn.style.color = 'red';
             }
             lbl[1].appendChild(spn);
         }
}

/**
 * ������ �������. ������ �� ����� � ���������.
 *
 */

function onInptFocus () {
    if (spn) {
        spn.style.display = 'none';
    }
}