let getUl = document.getElementsByClassName('main-ul');

if (getUl.length > 0) {
    getUl[0].innerHTML = createElements(['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv']);
    // getUl[0].innerHTML = createElements(['1', '2', '3', 'sea', 'user', 23]);
}

let timer = setInterval(function() {
    let span = document.getElementById('timer');
    let value = Number(span.innerText) - 1;

    if (value <= 0) {
        clearInterval(timer);

        getUl[0].remove();
    }

    span.innerHTML = Number(span.innerText) - 1;
}, 1000);

/**
 * ������� ��������� �� ���� ������ � ������� ��� � ���� ������ �� ��������
 *
 * @param list ������
 * @returns {arg is Array<any> | string | string}
 */
function createElements(list) {
    return Array.isArray(list) && list
        .map(function(item) {
            return `<li>${item}</li>`;
        })
        .join('') || '';
}

