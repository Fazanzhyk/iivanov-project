document.addEventListener('DOMContentLoaded', onReady);

const imgs = document.getElementsByClassName('image-to-show');
const inctv = document.getElementsByClassName('stop');
const rsm = document.getElementsByClassName('resume');
let cntr = 0;
let intrvl;
/**
 * Beginning of the Interval
 *
 */
function onReady () {
    inctv[0].style.display = 'inline-block';
    intrvl = setInterval(function () {
        cntr++;
         for (let i = 0; i < imgs.length; i++) {
             if (cntr === i) {
                 imgs[i].style.display = 'block';
             } else {
                 imgs[i].style.display = 'none';
             }
         }
        if (cntr === imgs.length-1) {
            cntr = -1;
        }
    }, 3000);
}
/**
 * Stop Interval
 *
 */
function onBtnStopClick () {
    rsm[0].style.display = 'inline-block';
    clearInterval(intrvl);
}
/**
 * Resume Interval
 *
 */
function onBtnStartClick () {
    onReady();
}





