const btns = document.querySelectorAll('button');
const divs = document.querySelectorAll('div');
let changeColorBtn;
let changeColorDiv;

function onBtnClick() {
   for (let btn of btns) {
       changeColorBtn = btn.classList.toggle('change_color_btns');
   }
   for (let div of divs) {
       changeColorDiv = div.classList.toggle('change_color_divs');
   }
    localStorage.setItem('change-Color-Btn', changeColorBtn);
    localStorage.setItem('change-Color-Div', changeColorDiv);
}

if (JSON.parse(localStorage.getItem('change-Color-Btn')) && JSON.parse(localStorage.getItem('change-Color-Div'))) {
    for (let btn of btns) {
        btn.classList.add('change_color_btns');
    }
    for (let div of divs) {
        div.classList.add('change_color_divs');
    }
}