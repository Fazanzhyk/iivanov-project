document.addEventListener('DOMContentLoaded', onReady);

function onReady () {
    const ul = document.querySelector('.tabs');
    ul.addEventListener('click', onLiClick);
}

/**
 * Tabs
 *
 * @param event
 */
function onLiClick (event) {
    const lisTitle = document.getElementsByClassName('tabs-title');
    const lisContent = document.getElementsByClassName('tabs-body');
    const dataTab = event.target.getAttribute('data-tab');
    for (let i = 0; i < lisTitle.length; i++) {
        lisTitle[i].classList.remove('active');
    }
    event.target.classList.add('active');
    for (let i = 0; i < lisContent.length; i++) {
        if (dataTab==i) {
            lisContent[i].style.display = 'block';
        } else {
            lisContent[i].style.display = 'none';
        }
    }
}