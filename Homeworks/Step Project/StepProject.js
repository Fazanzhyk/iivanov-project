document.addEventListener('DOMContentLoaded', onReady);

const filter = document.getElementsByClassName('tabs_imgs_header');
const filterImages = document.getElementsByClassName('tabs-imgs');
const clothes = document.getElementsByClassName('imageclothes');
const overImg = document.getElementsByClassName('mouse_over');
const right = document.getElementsByClassName('top_right');
const activeDiv = document.getElementsByClassName('active_div')
let loadMore;
const containers = document.getElementsByClassName('container');
const smallFoto = document.getElementsByClassName('small_foto');
let counter = 0;

/**
 *
 * ����������� ������������ �������
 */
function onReady () {
    for (let im of overImg) {
        im.addEventListener('mouseover', onMouseOver);
    }
    for (let im of overImg) {
        im.addEventListener('mouseout', onMouseOut);
    }
    const ul = document.querySelector('.top_tab');
    for (let fil of filter) {
        fil.addEventListener('click', filterClick);
    }
    ul.addEventListener('click', onLiClick);
    let btnLeft = document.getElementById('left');
    let btnRight = document.getElementById('right');
    btnLeft.addEventListener('click', onLeftClick);
    btnRight.addEventListener('click', onRightClick);
    loadMore = document.getElementById('load_more');
    loadMore.addEventListener('click', onLoadMoreClick);
    for (let foto of smallFoto) {
        foto.addEventListener('click', onFotoClick);
    }
}

/**
 * �������� �� ����� ������
 */
function onLeftClick () {
     counter--;
     if (counter<0) {
         counter=3;
     }
     for (let i =0; i < smallFoto.length; i++) {
         for (let j =0; j< containers.length; j++) {
             if (counter===j) {
                 containers[j].classList.add('show');
             } else {
                 containers[j].classList.remove('show');
             }
         }
         if (counter === i) {
             smallFoto[i].style.width = '80px';
             smallFoto[i].style.height = '80px';
         } else {
             smallFoto[i].style.width = '60px';
             smallFoto[i].style.height = '60px';
         }
         if (i > 3) {
             i = 0;
         }
     }
}

/**
 *
 * �������� �� ������ ������
 */
 function onRightClick () {
     counter++;
     if (counter > 3) {
         counter = 0;
     }
     for (let i = 0; i < smallFoto.length; i++) {
         for (let j =0; j< containers.length; j++) {
             if (counter===j) {
                 containers[j].classList.add('show');
             } else {
                 containers[j].classList.remove('show');
             }
         }
         if (counter === i) {
             smallFoto[i].style.width = '80px';
             smallFoto[i].style.height = '80px';
         } else {
             smallFoto[i].style.width = '60px';
             smallFoto[i].style.height = '60px';
         }
         if (i > 3) {
             i = 0;
         }
     }
 }
/**
 * Tabs
 *
 * @param event
 */
function onLiClick (event) {
    const lisTitle = document.getElementsByClassName('tabs_header');
    const lisContent = document.getElementsByClassName('tabs');
    const dataTab = event.target.getAttribute('data-tab');
    for (let i = 0; i < lisTitle.length; i++) {
        lisTitle[i].classList.remove('active');
    }
    event.target.classList.add('active');
    for (let i = 0; i < lisContent.length; i++) {
        if (dataTab==i) {
            lisContent[i].classList.add('active_li');
        } else {
            lisContent[i].classList.remove('active_li');
        }
    }
}

/**
 *
 * �������� �������������� ��������
 */
function onLoadMoreClick () {
    const nonActive = document.getElementsByClassName('non_active');
    const forJs = document.getElementById('forJs');
    for (let i = 0; i<nonActive.length;i++) {
            nonActive[i].classList.remove('non_active');

    }
    forJs.style.height = '1600px';
    loadMore.style.display = 'none';
}

/**
 * �������� �� ����� �� ����
 *
 * @param event
 */
function onFotoClick (event) {
    let trgt = event.target.getAttribute('data-target');
    for (let i = 0; i < smallFoto.length; i++) {
        smallFoto[i].style.width = '60px';
        smallFoto[i].style.height = '60px';
    }
    event.target.style.width = '80px';
    event.target.style.height = '80px';
    for (let j =0; j< containers.length; j++) {
        if (trgt==j) {
            containers[j].classList.add('show');
        } else {
            containers[j].classList.remove('show');
        }
    }
}

/**
 *
 * ������ ���������
 * @param event
 */
function filterClick (event) {
        let val = event.target.getAttribute('value');
    for (let div of activeDiv) {
        div.style.position = 'relative';
        div.style.top = '0';
    }
        for (let i = 0; i < filterImages.length; i++) {
            if (filterImages[i].classList.contains(val)) {
                filterImages[i].style.display = 'inline-block';
            } else {
                filterImages[i].style.display = 'none';
                for (let shirt of clothes) {
                    shirt.style.display = 'none';
                }
            }
        }
}

/**
 *
 * ������ Breaking News
 * @param event
 */
function onMouseOver (event) {
    let trgt = event.target.getAttribute('data-target');
    for (let i = 0; i < right.length; i++) {
            if (trgt == i) {
                right[i].style.backgroundColor = '#18cfab';
            } else {
                right[i].style.backgroundColor = '#203e38';
            }
        }
    }
function onMouseOut () {
    for (let i = 0; i < right.length; i++) {
            right[i].style.backgroundColor = '#203e38';
    }
}