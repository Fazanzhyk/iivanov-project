document.addEventListener('DOMContentLoaded', onReady);



function onReady () {
    const btnToggle = document.getElementById('btnToggle');
    btnToggle.addEventListener('click', onBtnToggleClick);
}

function onBtnToggleClick (event) {
    const target = event.currentTarget;
    const ulMenu = document.querySelector('.top-nav__list');

    if (ulMenu) {
        ulMenu.classList.toggle('active');
    }
    if (ulMenu.classList.contains('active')) {
        target.innerHTML = 'X';
    }else {
        target.innerHTML = '<span class="fas fa-bars"></span>';
    }
}
