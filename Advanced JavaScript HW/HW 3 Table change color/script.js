document.addEventListener('DOMContentLoaded', onReady);

class Table {
    constructor(id) {
        this.id = id;
    }

    build (element) {
        return document.createElement(element);
    }
}

class TR extends Table{
    build () {
        return super.build ('tr');
    }
}

class TD extends Table {
    build () {
        return super.build ('td');
    }
}

let table = new Table ('main');
let tr = new TR;
let td = new TD;


function onReady() {
    let tMain = table.build('table');
    tMain.addEventListener('click', onTableClick);
    document.body.appendChild(tMain);
    for (let i = 0; i < 30; i++) {
        let trMain = tr.build();
        tMain.appendChild(trMain);
        for (let j = 0; j < 30; j++) {
            let tdMain = td.build();
            trMain.appendChild(tdMain);
        }
    }
}

function onTableClick (e) {
    e.stopPropagation();
    let target = e.target;
    target.classList.toggle('active');
}

function onBodyClick (e) {
    let tbl = document.getElementsByTagName('table');
    tbl[0].classList.toggle('active');
}