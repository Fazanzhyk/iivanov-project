
class HamburgerException {
    constructor (message) {
        this.message = message;
        alert (this.message);
    }
}

class Hamburger  {
    constructor (size, stuffing) {
        if (size === Hamburger.SIZE_SMALL || size === Hamburger.SIZE_LARGE) {
            this.size = size;
        } else {
            throw new HamburgerException(`Invalid size: ${this.size}`);
        }
        if (stuffing === Hamburger.STUFFING_CHEESE || stuffing === Hamburger.STUFFING_SALAD || stuffing === Hamburger.STUFFING_POTATO) {
            this.stuffing = stuffing;
        } else {
            throw new HamburgerException(`Invalid stuffing: ${this.stuffing}`);
        }
        this.toppings = [];
        if (this.size === undefined || this.stuffing === undefined) {
            throw new HamburgerException('No size or stuffing given');
        }
    }

    addTopping (topping) {
        if (this.toppings.length === 0) {
            this.toppings.push(topping);
        }else {
            if (this.toppings.indexOf(topping) === -1) {
                this.toppings.push(topping);
                return true;
            } else {
                return new HamburgerException(`Duplicate topping: ${topping}`);
            }
        }
    }

    removeTopping (topping) {
        if (this.toppings.indexOf(topping) !== -1) {
            this.toppings.splice(this.toppings.indexOf(topping), 1);
            return true;
        }
        return false;
    }

    getToppings () {
        return this.toppings;
    }

    getSize () {
        return this.size;
    }

    getStuffing () {
        return this.stuffing;
    }

    calculatePrice () {
        let cost = this.size.cost + this.stuffing.cost;
        if (this.toppings.length === 1) {
            return cost + this.toppings[0].cost + ' ' + 'UAH';
        }
        if (this.toppings.length > 1) {
            return cost + this.toppings[0].cost + this.toppings[1].cost + ' ' + 'UAH';
        }
        return cost + ' ' + 'UAH';
    }

    calculateCalories () {
        let calories = this.size.calories + this.stuffing.calories;
        if (this.toppings.length === 1) {
            return calories + this.toppings[0].calories + ' ' + 'Kkal';
        }
        if (this.toppings.length > 1) {
            return calories + this.toppings[0].calories + this.toppings[1].calories + ' ' + 'Kkal';
        }
        return calories + ' ' + 'Kkal';
    }
}
/**
 * Размеры, виды начинок и добавок
 * */

Hamburger.SIZE_SMALL = {
    cost: 50,
    calories: 20
};

Hamburger.SIZE_LARGE = {
    cost: 100,
    calories: 40
};

Hamburger.STUFFING_CHEESE = {
    cost: 10,
    calories: 20
};

Hamburger.STUFFING_SALAD = {
    cost: 20,
    calories: 5
};

Hamburger.STUFFING_POTATO = {
    cost: 15,
    calories: 10
};

Hamburger.TOPPING_MAYO = {
    cost: 20,
    calories: 5
};

Hamburger.TOPPING_SPICE = {
    cost: 15,
    calories: 0
};