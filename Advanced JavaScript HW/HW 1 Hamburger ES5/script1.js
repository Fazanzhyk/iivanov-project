/**
 * Класс, объекты которого описывают параметры гамбургера.
 *
 * @constructor
 * @param size        Размер
 * @param stuffing    Начинка
 * @throws {HamburgerException}  При неправильном использовании
 */

function Hamburger(size, stuffing) {
    if (size === Hamburger.SIZE_SMALL || size === Hamburger.SIZE_LARGE) {
        this.size = size;
    } else {
        throw new HamburgerException(`Invalid size: ${this.size}`);
    }
    if (stuffing === Hamburger.STUFFING_CHEESE || stuffing === Hamburger.STUFFING_SALAD || stuffing === Hamburger.STUFFING_POTATO) {
        this.stuffing = stuffing;
    } else {
        throw new HamburgerException(`Invalid stuffing: ${this.stuffing}`);
    }
        this.toppings = [];
    if (this.size === undefined || this.stuffing === undefined) {
        throw new HamburgerException('No size or stuffing given');
    }
};

/**
 * Размеры, виды начинок и добавок
 * */

Hamburger.SIZE_SMALL = {
    cost: 50,
    calories: 20
};

Hamburger.SIZE_LARGE = {
    cost: 100,
    calories: 40
};

Hamburger.STUFFING_CHEESE = {
    cost: 10,
    calories: 20
};

Hamburger.STUFFING_SALAD = {
    cost: 20,
    calories: 5
};

Hamburger.STUFFING_POTATO = {
    cost: 15,
    calories: 10
};

Hamburger.TOPPING_MAYO = {
    cost: 20,
    calories: 5
};

Hamburger.TOPPING_SPICE = {
    cost: 15,
    calories: 0
};

/**
 * Добавить добавку к гамбургеру. Можно добавить несколько
 * добавок, при условии, что они разные.
 *
 * @param topping     Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */

Hamburger.prototype.addTopping = function (topping) {
    if (this.toppings.length === 0) {
        this.toppings.push(topping);
    }else {
        if (this.toppings.indexOf(topping) === -1) {
            this.toppings.push(topping);
            return true;
        } else {
            return new HamburgerException(`Duplicate topping: ${topping}`);
        }
    }
};

/**
 * Убрать добавку, при условии, что она ранее была
 * добавлена.
 *
 * @param topping   Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */

Hamburger.prototype.removeTopping = function (topping) {
    if (this.toppings.indexOf(topping) !== -1) {
        this.toppings.splice(this.toppings.indexOf(topping), 1);
        return true;
    }
    return false;
};

/**
 * Получить список добавок.
 *
 * @return {Array} Массив добавленных добавок, содержит константы
 *                 Hamburger.TOPPING_*
 */

Hamburger.prototype.getToppings = function () {
    return this.toppings;
};

/**
 * Узнать размер гамбургера
 */

Hamburger.prototype.getSize = function () {
    return this.size;
};

/**
 * Узнать начинку гамбургера
 */

Hamburger.prototype.getStuffing = function () {
    return this.stuffing;
};

/**
 * Узнать цену гамбургера
 * @return {Number} Цена в тугриках
 */

Hamburger.prototype.calculatePrice = function () {
    let cost = this.size.cost + this.stuffing.cost;
    if (this.toppings.length === 1) {
       return cost + this.toppings[0].cost + ' ' + 'UAH';
    }
    if (this.toppings.length > 1) {
        return cost + this.toppings[0].cost + this.toppings[1].cost + ' ' + 'UAH';
    }
    return cost + ' ' + 'UAH';
};

/**
 * Узнать калорийность
 * @return {Number} Калорийность в калориях
 */

Hamburger.prototype.calculateCalories = function () {
    let calories = this.size.calories + this.stuffing.calories;
    if (this.toppings.length === 1) {
        return calories + this.toppings[0].calories + ' ' + 'Kkal';
    }
    if (this.toppings.length > 1) {
        return calories + this.toppings[0].calories + this.toppings[1].calories + ' ' + 'Kkal';
    }
    return calories + ' ' + 'Kkal';
};

/**
 * Представляет информацию об ошибке в ходе работы с гамбургером.
 * Подробности хранятся в свойстве message.
 * @constructor
 */

function HamburgerException (message) {
    this.message = message;
    alert (this.message);
};