package com.iigor791;

import java.util.Random;
import java.util.Scanner;

/**
 * Class GuessNumber provides ability to play a game with computer. Here the main method established, where user able to
 * negotiate with AI via Console
 *
 * @version  1.0 23 Jan 2020
 * @author   Igor Ivanov
 *
 */

public class GuessNumber {
    /*4 String constants to negotiate with User via console*/
    final static String START = "Let the game begins!";
    final static String NUMBER_QUERY = "Enter your number: ";
    final static String BIG_NUMBER = "Your number is too big! Please, try again...";
    final static String SMALL_NUMBER = "Your number is too small! Please, try again...";

    /*length - means number of tries in guessing games. For example, it can be 5, so the user will have only 5 tries
    * to guess a number
    * */
    public static int length = 101;
    public static int [] numbers = new int [length];

    /*main method provides ability to play*/
    public static void main(String[] args) {

        /*val saves the random number*/
        int val = Game.randomize();
        /*ask User name and save it to User.name*/
        User.setName();

        System.out.printf("%s \n", START);

        Scanner scanner = new Scanner(System.in);

        for (int i = 0; i < length; i++) {
            System.out.printf("%s \n", NUMBER_QUERY);
            numbers[i] = scanner.nextInt();

            if (numbers[i] > val) {
                System.out.printf("%s \n", BIG_NUMBER);
            } else if (numbers[i] < val) {
                System.out.printf("%s \n", SMALL_NUMBER);
            } else {
                System.out.printf("Congratulations, %s! You have won! \n", User.name);
                break;
            }
        }
    }
}

/*class User created to do some operations with User name, to make main code much better readable*/
class User {

    public static String name;
    /*setName method asks user name and saves it to name*/
    public static void setName() {
        Scanner scanner = new Scanner (System.in);
        System.out.println("Enter your name: ");
        name = scanner.nextLine();
    }
}

/*via this class Game programme makes up the random number*/
class Game {

    public static int randomize() {
        Random random = new Random();
        return random.nextInt(101);
    }
}

/*For future updates (for myself)*/
//        Не обязательное задание продвинутой сложности:
//
//        Перед переходом на следующую итерацию, программа сохраняет введенное пользователем число в массив. После того
//        как игрок угадал загаданное число, перед выходом программа выводит на экран текст: Your numbers:  и показывает все
//        введенные игроком числа, отсортированные от большего к меньшему.
//        После ввода чисел пользователем добавить проверку их корректности. Если пользователь ввел не число - спросить заново.
//        Добавьте игре немного больше смысла: пускай загаданное число будет годом, которому соответствует известное событие.
//        Информация о годах хранится в двумерном массиве [год Х событие]. Программа вначале выбирает случайным образом ячейку
//        в матрице и выводит на экран: When did the World War II begin?.