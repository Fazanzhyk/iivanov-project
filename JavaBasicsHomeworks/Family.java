package com.iigor791.hw5;

/*
 * Class Family provides ability to create instances of Family. It contains 5 private fields, 2 final Strings to negotiate via Console,
 * 3 methods (add/deleteChild & countFamily()), 1 constructor, and 3 overrided methods.
 *
 * @version  1.0  2 Feb 2020
 * @author   Igor Ivanov
 *
 * */

import java.util.Arrays;

public class Family {

    private int id;
    private Human mother;
    private Human father;
    private Human [] children;
    private Pet pet;

    final String ANNOUNCEMENT = "New Object had been created of type Family!";
    final static String UPLOADING = "Class Family is uploading! Please, wait a bit!";

    public Human getMother () {
        return mother;
    }
    public Human getFather () {
        return father;
    }
    public String getChildren () {
        return Arrays.toString(children);
    }
    public Pet getPet () {
        return pet;
    }
    public int getId () {
        return id;
    }

    public void setMother (Human mother) {
        this.mother = mother;
    }
    public void setFather (Human father) {
        this.father = father;
    }
    public void setChildren (Human [] args) {
        this.children = args;
    }
    public void setPet (Pet pet) {
        this.pet = pet;
    }
    public void setId (int id) {
        this.id = id;
    }

    public void addChild (Human human) {
        int length = children.length + 1;
        Human [] arr = new Human [length];
        for (int i = 0; i < children.length; i++) {
            arr[i] = children[i];
        }
        for (int j = 0; j < arr.length; j++) {
            if (arr[j] == null) {
                arr[j] = human;
            }
        }
        human.setId(hashCode());
        human.setFamily(this);
        setChildren(arr);
    }

    public boolean deleteChild (int index) {
        int length = children.length - 1;
        Human [] arr = new Human [length];
        for (int i = 0; i < index; i++) {
            arr[i] = children[i];
        }
        for (int i = length; i > index; i--) {
            arr[i] = children[i];
        }
        children[index].setFamily(null);
        children[index].setId(0);
        if (children[index].getId() == 0) {
            setChildren(arr);
            return true;
        } else {
            return false;
        }
    }

    public int countFamily () {
        return 2 + children.length;
    }

    static {
        System.out.println(UPLOADING);
    }

    {
        System.out.println(ANNOUNCEMENT);
    }

    public Family (int id, Human mother, Human father, Pet pet, Human ...args) {
            setId(id);
            setMother(mother);
            setFather(father);
            setPet(pet);
            setChildren(args);
            mother.setId(this.hashCode());
            mother.setFamily(this);
            father.setId(this.hashCode());
            father.setFamily(this);
            pet.setId(this.hashCode());
        for (Human arg : args) {
            arg.setId(this.hashCode());
            arg.setFamily(this);
        }

    }

    @Override
    public String toString() {
        return "Mother: "+mother.toString()+"Father: "+father.toString()+"Children: "+ Arrays.toString(children) +"Pet: " + pet.toString();
    }

    @Override
    public int hashCode() {
        return getId() * 2 + 1;
    }

    @Override
    public boolean equals (Object obj) {
        if (!(obj instanceof Family)) {
            return false;
        }
        return this.hashCode() == obj.hashCode();
    }
}
