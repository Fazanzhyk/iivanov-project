package com.iigor791;

import java.util.Random;
import java.util.Scanner;

/*
 * Class SapperGame provides ability to play Sapper Game with computer. Via main method of this class all logic set
 *
 * @version   1.0 27 January 2020
 * @author    Igor Ivanov
 *
 **/

public class SapperGame {

    // for data validation
    static boolean isDigit(String value) {
        return value.matches("\\d+");
    }

    public static void main(String[] args) {

        int start = SapperGameSettings.guessField(1, 6);
        int end = SapperGameSettings.guessField(1, 6);

        System.out.printf("%s \n", SapperGameSettings.GAME_START);

        Scanner scanner = new Scanner(System.in);

        // number 20 uses for number of tries in the game
        for (int i = 0; i < 21; i++) {

            System.out.printf("%s \n", SapperGameSettings.GUESS_ROW);
            String row = scanner.next();

            if (isDigit(row) && (Integer.parseInt(row) >= 1) && (Integer.parseInt(row) <= 5)) {

                System.out.printf("%s \n", SapperGameSettings.GUESS_COLUMN);
                String column = scanner.next();

                if (isDigit(column) && (Integer.parseInt(column) >= 1) && (Integer.parseInt(column) <= 5)) {

                    if ((Integer.parseInt(row) == start) && (Integer.parseInt(column) == end)) {

                        SapperGameSettings.field[Integer.parseInt(row)][Integer.parseInt(column)] = "x";
                        System.out.printf("%s \n", SapperGameSettings.GAME_END);
                        SapperGameSettings.printArr(SapperGameSettings.field);
                        break;

                    } else {
                        SapperGameSettings.field[Integer.parseInt(row)][Integer.parseInt(column)] = "*";
                        SapperGameSettings.printArr(SapperGameSettings.field);
                    }
                }
            }
        }
    }
}