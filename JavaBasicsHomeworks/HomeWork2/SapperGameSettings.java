package com.iigor791;

import java.util.Random;
import java.util.Scanner;

// Class SapperGameSettings contains all Settings which are need for game

class SapperGameSettings {
    // final expressions for negotiations with User
    final static String GAME_START = "All set. Get ready to rumble!";
    final static String GUESS_ROW = "Enter row number from 1 to 5: ";
    final static String GUESS_COLUMN = "Enter column number from 1 to 5: ";
    final static String GAME_END = "You have won!";

    // Field where Game will run.
    public static String [][] field = {
            {"0", "1", "2", "3", "4", "5"},
            {"1","-","-","-","-","-"},
            {"2","-","-","-","-","-"},
            {"3","-","-","-","-","-"},
            {"4","-","-","-","-","-"},
            {"5","-","-","-","-","-"},
    };

    // randomly guess row & column numbers
    static int guessField (int start, int end) {
        Random random = new Random ();
        return random.ints(start, end).findFirst().getAsInt();
    }

    // printArr provides ability to print field to Console after each iteration in main method
    static void printArr (String [][] arr) {

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                System.out.print(arr[i][j] + " / ");
            }
            System.out.println();
         }
    }
}