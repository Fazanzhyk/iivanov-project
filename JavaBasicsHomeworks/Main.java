package com.iigor791.hw5;

/*
* This is simple runner Class - via which the app runs
*
* @version 2.0 2 Feb 2020
* @author  Igor Ivanov
* */

import java.util.Random;

public class Main {

    public static void main(String[] args) {

        Human alla = new Human ("Alla", "Pugacheva", 1949);
        Human filip = new Human ("Filip", "Kirkorov", 1960);
        Human kris = new Human ("Krystina", "Orbakayte", 1971);
        Human maxim = new Human ("Maxim", "Galkin", 1982);
        Pet sharik = new Pet ("dog", "Sharik", 7, 44, new String[]{"barkle", "howl", "sleep"});

        Family arlekinoFamily = new Family (1, alla, filip, sharik, kris, maxim);
//        System.out.println(arlekinoFamily.toString());
//        System.out.println(arlekinoFamily.hashCode());
//        System.out.println(alla.getId());
//        System.out.println(filip.getId());
//        System.out.println(kris.getId());
//        System.out.println(maxim.getId());
//        System.out.println(sharik.getId());
//        System.out.println(arlekinoFamily.equals(alla));

        Human borka = new Human ("Borys", "Moiseev", 1955);

        arlekinoFamily.addChild(borka);
        System.out.println(arlekinoFamily.countFamily());
        System.out.println(borka.getId());

        Random random = new Random();
        int random1 = random.nextInt(2);
        int random2 = random.nextInt(100);
        boolean bool = sharik.isTimeToEat(random1, random2);
        System.out.println(borka.feedPet(bool));
        System.out.println(arlekinoFamily.toString());

//        Object obj = new Object();
//        System.out.println(obj.hashCode());

        sharik.respond();
        maxim.greetPet();
        kris.describePet();

        arlekinoFamily.deleteChild(2);
        System.out.println(arlekinoFamily.countFamily());
        System.out.println(arlekinoFamily.toString());


    }
}

//    for future updates (for myself)
//    В Family опишите метод удалить ребенка(deleteChild) (принимает тип Human и удаляет соответствующий элемент). Метод должен быть написан
//        с учетом наличия методов equals() и hashCode().
//        Подсказка: для того, чтобы удалить верный элемент из массива Human'ов вам необходимо делать сравнения по полям идентифицирующим
//        именно данного человека (подумайте, какие поля для этого подходят).